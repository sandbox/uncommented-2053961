Overflowing Image

Summary

This module provides an image display formatter that allows a selected  region
of an image to occupy space and maintain aspect ratio as if it were a  smaller
image. The rest of the image remains free to expand outwards.  This is
particularly useful when dealing with responsive designs that  incorporate large
splash images, where on some pages the entire image should scale down and on
some pages the image should be cropped as screen  width decreases.


Possible Use Cases:

- You have a large area to fill with an image (such as a slide), and you want
  a selected region of the image to always be displayed while the rest can
  expand/collapse as-needed.

- You have content maintainers who frequently want to change the background
  image of a region of your responsive design, and you don't want to get stuck
  coding special cases for every page update.

  
Usage

- Enable Imagefield Focus, and create an image field with "Enable focus" checked
  on your content type or entity of choice.
 
- On the "Manage Display" tab for said content type or entity, set the format
  for your chosen image field to "Overflowing Image".

- Create content using your newly-configured content (or entity) type, and
  specify a focus rectangle.
 

How it works

This field formatter encases an image in a div that automatically maintains
it's aspect ratio (see Responsive Elements that retain their aspect ratio),  and
positions said image absolutely based on percent offset within said div.  This
allows the container div to behave in much the same way a normal image  would in
terms of page layout, and the image's position and scale to exist  relative to
that.
